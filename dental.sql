-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 15, 2016 at 09:20 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dental`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

DROP TABLE IF EXISTS `user_login`;
CREATE TABLE IF NOT EXISTS `user_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `screenname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(50) NOT NULL,
  `role` int(11) NOT NULL COMMENT '1-superadmin,2-admin,3-doctor',
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `user_login`
--

TRUNCATE TABLE `user_login`;
--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`id`, `screenname`, `username`, `password`, `created_at`, `created_by`, `updated_at`, `updated_by`, `role`, `status`) VALUES
(1, 'super admin', 'sadmin', '6a0f8565aad668a1f76d94d1c9c9faa82fbd094aeece43818c3440afdd01f1da', '2016-06-15 05:13:10', 'sadmin', '0000-00-00 00:00:00', '', 1, 1),
(2, 'admin', 'admin', '7676aaafb027c825bd9abab78b234070e702752f625b752e55e55b48e607e358', '2016-06-15 06:03:39', 'sadmin', '0000-00-00 00:00:00', '', 2, 1),
(3, 'doctor', 'doctor', '84b34ae682e666973afb47dca4247d12603670be21a59dbbb53bdcd3db373dde', '2016-06-15 06:04:19', 'sadmin', '0000-00-00 00:00:00', '', 3, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
