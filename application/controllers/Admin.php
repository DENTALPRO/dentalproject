<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function index()
	{
		if(empty($this->session->userdata('username'))){
		$this->load->view('login');
		}else{
			redirect('admin/dashboard');
		}
	}


	function dashboard(){
		$this->load->view('admin/dashboard');
	}

}