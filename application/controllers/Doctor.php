<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor extends CI_Controller {

	public function index()
	{
		if(empty($this->session->userdata('username'))){
		$this->load->view('login');
		}else{
			redirect('Doctor/dashboard');
		}
	}


	function dashboard(){
		$this->load->view('Doctor/dashboard');
	}

}