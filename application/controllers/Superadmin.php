<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Superadmin extends CI_Controller {

	public function index()
	{
		if(empty($this->session->userdata('username'))){
		$this->load->view('login');
		}else{
			redirect('superadmin/dashboard');
		}
	}


	function dashboard(){
		$this->load->view('superadmin/dashboard');
	}

}