<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		if(empty($this->session->userdata('username'))){
		$this->load->view('login');
		}else{
			$this->dashboard(true);
		}
	}
	
	function login_validate(){
		$data = array('username'=>$this->input->post('form-username'),'password'=>hash("sha256",$this->input->post('form-password')));
		$login = $this->login_model->getlogindata(json_encode($data));	

				if(count($login) == 1){
				if($login->role == "1"){
					$sess = array('username'=>$login->username,'role'=>$login->role,'screenname'=>$login->screenname);
					$this->session->set_userdata($sess);
					echo '1';
				}else if($login->role == "2"){
					$sess = array('username'=>$login->username,'role'=>$login->role);
					$this->session->set_userdata($sess);
					echo '2';
				}elseif($login->role == "3"){
					$sess = array('username'=>$login->username,'role'=>$login->role);
					$this->session->set_userdata($sess);
					echo '3';
				}else{
					echo "Sorry.Your role is not decided.Please contact super admin";
				}
			}else if(count($login) == 0){
				echo '0';
			}else{
				$this->db->error();
			}
	}
	
	function dashboard($ul){
		if($this->session->userdata('role') == '1'){
			redirect('superadmin/dashboard');
		}else if($this->session->userdata('role') == '2'){
			redirect('admin/dashboard');
		}else if($this->session->userdata('role') == '3'){
			redirect('doctor/dashboard');
		}else{
			$this->logout(true);
		}
		
	}
	
	function logout(){
		$this->session->sess_destroy();
		redirect('/');
	}
	
	
	
}
