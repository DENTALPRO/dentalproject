<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {
	
	function getlogindata($data){
		$dat = json_decode($data);
		$q = $this->db->where('username',$dat->username)->where('password',$dat->password)->get('user_login');
		return $q->row();
	}
	
}