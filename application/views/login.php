<?php
defined('BASEPATH') OR exit('No direct script access allowed');
echo doctype('html5');
?>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login Form</title>

        <!-- CSS -->
		<?php 
		echo link_tag('http://fonts.googleapis.com/css?family=Roboto:400,100,300,500');
		echo link_tag('assets/bootstrap/css/bootstrap.min.css');
		echo link_tag('assets/font-awesome/css/font-awesome.min.css');
		echo link_tag('assets/css/form-elements.css');
		echo link_tag('assets/css/styles.css');	
		echo link_tag('assets/mycss/font-awesome.min.css');		
		?>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
		<style>
#box {
    box-shadow: -6px 8px 54px rgba(0, 0, 0, 0.32);
}
.post_images{
	margin-left:-20px !important;
}
.loginerror{
	border: 1px solid red;
}
.loginok{
	border: 1px solid #31708f;
}
</style>

    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <!--<h1><strong>Ahuja Dental Clinic</strong></h1>-->
							<?php 
$image_properties = array('src'=> base_url().'assets/img/logo.jpg','alt' =>'Logo','class' => 'post_images','width' => '400','height'=> '100','title' => 'LOGO','rel'=>'lightbox');
?>
                            <h3><a href="#"><?php echo img($image_properties); ?></a></h3>
							
                        </div>
                    </div>
					
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4 form-box" id="box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h4 style="color:#dd4b39">Login Form</h4>
                            		<!--<p style="color:#31708f"></p>-->
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-key" style="color:#dd4b39;"></i>
                        		</div>
                            </div>
                            <div class="form-bottom" style="padding:0px 25px 24px 25px;">
			                  
		<p><i class="fa fa-spin fa-spinner suc" style="display:none;"></i> &nbsp;&nbsp;<span class="loginsuc"></span> </p>
								
								<?php
								$attributes = array('class' => 'login-form', 'name'=>'myform','id' => 'myform','role'=>'form');
								echo form_open('#',$attributes); ?>
			                    	<div class="form-group">
									<?php
$attr = array('type'=>'text','name'=>'form-username','placeholder'=>'Username','class'=>'form-control','id'=>'form-username');
										echo form_input($attr);
										
			echo form_hidden("_token",hash("sha256","Login"));
										?>
										<span class="form-username"></span>
			                        </div>
			                        <div class="form-group">
										<?php
$at = array('type'=>'password','name'=>'form-password','placeholder'=>'Password','class'=>'form-control','id'=>'form-password');
										echo form_input($at); ?>
										<span class="form-password"></span>
			                        </div>
			                   
									<?php 
									$js = 'onClick="return formvalidate()"';
								//$data = array('class'=>'btn',$js);	
		
									echo form_submit('submit','Login','class="btn" id="login" onclick="return validate();"'); 
									echo '<i class="fa fa-spin fa-spinner loader" style="display:none;"></i>';
									echo form_close(); ?>
		                    </div>
                        </div>
                    </div>
					
                </div>
            </div>
            
        </div>

        <!-- Javascript -->
        <script src="<?php echo base_url();?>assets/myjquery/jquery.min.js"></script>
		<!--<script src="http://malsup.github.io/jquery.corner.js"></script> -->
        <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.backstretch.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/scripts.js"></script>
		<script src="<?php echo base_url();?>assets/myjquery/login.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->
		

    </body>

</html>