"use strict";

var base_url = "http://localhost/dental/";

function validate(){
			//event.preventDefault();
			
		var username = $("#form-username").val();
		var password = $("#form-password").val();
		var er=[];
		var cnt = 0;
		
		if(username.length == ""){
			$(".form-username").css('color','red').html('Please enter username');
			er+=cnt+1;
		}else{
			$(".form-username").html('');
		}
		
		if(password == ""){
			$(".form-password").css('color','red').html('Please enter password');
			er+=cnt+1;
		}else{
			$(".form-password").html('');
		}
		
		if(er != ""){
			return false;
		}
		
		var Data = $("#myform").serializeArray();
		 console.log(Data);
		 
		$.ajax({
        url: 'login-validate',
        type: "POST",
        data: Data,
		beforeSend: function(){$(".loader").show(); $("#login").prop('disabled',true); },
        success: function (re) {
			if(re == 1){
				$(".loader").hide(); 
				$(".suc").show(); 
				$(".loginsuc").css('color','green').html('Please wait you are logging in');
				setTimeout('window.location.assign("'+base_url+'superadmin/dashboard")',2000);
			}else if(re == 2){
				$(".loader").hide(); 
				$(".suc").show(); 
				$(".loginsuc").css('color','green').html('Please wait you are logging in');
				setTimeout('window.location.assign("'+base_url+'admin/dashboard")',2000);
			}else if(re == 3){
				$(".loader").hide(); 
				$(".suc").show(); 
				$(".loginsuc").css('color','green').html('Please wait you are logging in');
				setTimeout('window.location.assign("'+base_url+'doctor/dashboard")',2000);
			}else if(re == 0){
				$(".loader").hide(); 
				$("#login").prop('disabled',false);
				$(".loginsuc").css('color','red').html('Invalid username or password');
			}else{
				$(".loader").hide(); 
				$("#login").prop('disabled',false);
				$(".loginsuc").css('color','red').html('Unable to login.Please try again after some time');
			}
           
			
        }
    });
			
		}